import urllib2
import sqlite3
conn = sqlite3.connect('data.db')
conn.text_factory = str
c = conn.cursor()
c.execute('SELECT * FROM pages WHERE length>5000 ORDER BY length')
f = open('output.md', 'w')
for item in c.fetchall():
    f.write('[' + item[1] + ': ' + str(item[2]) + ' words](' + item[0] + ')\r\n\r\n')