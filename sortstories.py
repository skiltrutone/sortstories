import urllib2
import sqlite3
conn = sqlite3.connect('data.db')
conn.text_factory = str
c = conn.cursor()
req = urllib2.Request('http://mcstories.com/Tags/md.html')
response = urllib2.urlopen(req)
the_page = response.read()
lines = the_page.split('\n')
badTagList = ["mm", "cb", "be", "fd", "ft", "hu", "ws", "fu"]
goodList = []
goodLenList = []
pageList = []
i = 1
inTable = False
for num,line in enumerate(lines):
    if "</table>" in line:
        inTable = False
    if inTable and num%2==1:
        url = "http://mcstories.com/" + line.split('"')[3].split('../')[1]
        name = line.split('<cite>')[1].split('</cite>')[0]
        #print url
        #print name
    if inTable and num%2==0:
        tags = line.split('<td>')[1].split('</td>')[0].split(' ')
        pageList.append([url,name,tags])
    if "<table" in line:
        inTable = True
for item in pageList:
    if not set(badTagList).intersection(item[2]):
        goodList.append(item)
f = open('output.html', 'w')
for item in goodList:
#item = goodList[0]
    req = urllib2.Request(item[0])
    response = urllib2.urlopen(req)
    the_page = response.read()
    if '<div class="chapter"><a href=' in the_page:
        newLink = the_page.split('<div class="chapter"><a href="')[1].split('">')[0]
        item[0] = item[0].replace("index.html", newLink)
        storyLen = the_page.split(' words)</div>')[0].split('(')
        item.append(str(storyLen[len(storyLen)-1]))
    else:
        totalLen = 0
        pageLen = the_page.split(' words</td>')
        for i in range(0,len(pageLen)-1):
            totalLen += int(pageLen[i].split('<td>')[len(pageLen[i].split('<td>'))-1])
        item.append(str(totalLen))
        newLink = the_page.split('<tr><td><a href="')[1].split('">')[0]
        item[0] = item[0].replace("index.html", newLink)
    print "INSERT INTO pages VALUES (" + item[0] + "," + item[1] + "," + item[3] + ")"
    c.execute("""INSERT INTO pages(url, name, length)
        VALUES (?,?,?);""", (item[0], item[1], item[3]))
    conn.commit()
conn.close